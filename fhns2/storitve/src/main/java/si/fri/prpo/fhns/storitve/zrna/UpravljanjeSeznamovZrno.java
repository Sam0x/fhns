package si.fri.prpo.fhns.storitve.zrna;

import si.fri.prpo.fhns.entitete.Artikel;
import si.fri.prpo.fhns.entitete.OpisArtiklov;
import si.fri.prpo.fhns.entitete.Seznam;
import si.fri.prpo.fhns.entitete.Uporabnik;
import si.fri.prpo.fhns.storitve.dto.ArtikelDTO;
import si.fri.prpo.fhns.storitve.dto.DodajArtikelNaSeznamDTO;
import si.fri.prpo.fhns.storitve.dto.SeznamDTO;
import si.fri.prpo.fhns.storitve.napake.NeveljavenZahtevekException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

@RequestScoped
public class UpravljanjeSeznamovZrno {

    Logger log = Logger.getLogger(UpravljanjeSeznamovZrno.class.getName());

    private UUID uuid;

    @Inject
    private UporabnikiZrno uz;
    @Inject
    private SeznamZrno sz;
    @Inject
    private ArtikelZrno az;

    @Transactional
    public void dodajSeznam(SeznamDTO sez){
        log.info("RequestScoped UUID: " + uuid.toString());
        Seznam seznam = new Seznam(sez.getNaziv(), sez.getOpis(), sez.getStatus(), null , null);

        Uporabnik u = uz.getUporabnik(sez.getUporabnikId());
        List<Seznam> sezList = u.getSeznami();
        if(sezList == null){
            sezList = new ArrayList<Seznam>();
        }
        sezList.add(seznam);
        u.setSeznami(sezList);

        if(sez.getArtikli()!=null){
            List<Artikel> art = new ArrayList<Artikel>();
            for (int a : sez.getArtikli()){
                art.add(az.getArtikel(a));
            }
            seznam.setArtikli(art);
        }else{
            seznam.setArtikli(null);
        }

        uz.updateUporabnik(u);
    }

    @Transactional
    public void odstraniKoncaneSezname(){
        log.info("RequestScoped UUID: " + uuid.toString());
        List<Seznam> vsi = sz.getSeznam();
        for(Seznam s : vsi){
            if(s.getStatus().equals("Zakljucen")){
                sz.deleteSeznam(s.getId());
            }
        }
    }

    @Transactional
    public void dodajNovArtikel(ArtikelDTO art){
        log.info("RequestScoped UUID: " + uuid.toString());
        az.createArtikel(new Artikel(art.getNaziv(), new OpisArtiklov(), art.getStatus(), null));
    }

    @PostConstruct
    public void logInit(){
        log.info("Inicializacija UpravljanjeSeznamovZrno!");
        uuid = UUID.randomUUID();
    }

    @Transactional(value = Transactional.TxType.REQUIRES_NEW)
    public boolean dodajArtikelNaSeznam(DodajArtikelNaSeznamDTO dto) throws NeveljavenZahtevekException {
        Seznam seznam = sz.getSeznam(dto.getSeznamId());
        if (seznam == null) {
            return false;
        }
        // ustvari nov artikel
        Artikel artikel = new Artikel();
        artikel.setNaziv(dto.getArtikelNaziv());
        artikel.setOpisArtiklov(dto.getArtikelOpis());
        artikel.setStatus("Aktiven");
        artikel.setSeznami(seznam);
        az.createArtikel(artikel);

        seznam.getArtikli().add(artikel);
        sz.updateSeznam(seznam);
        return true;
    }

    @PreDestroy
    public void logDestroy(){
        log.info("Unicenje UpravljanjeSeznamovZrno!");
    }
}
