package si.fri.prpo.fhns.storitve.zrna;

import com.kumuluz.ee.rest.beans.QueryParameters;
import com.kumuluz.ee.rest.utils.JPAUtils;
import si.fri.prpo.fhns.entitete.Uporabnik;
import si.fri.prpo.fhns.storitve.interceptorji.BeleziKlice;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;
import java.util.logging.Logger;


@ApplicationScoped
public class UporabnikiZrno{

    Logger log = Logger.getLogger(UporabnikiZrno.class.getName());

    @PersistenceContext(unitName = "fhns2-jpa")
    private EntityManager em;

    @BeleziKlice
    @Transactional
    public List<Uporabnik> getUporabniki(QueryParameters query) {
        return JPAUtils.queryEntities(em, Uporabnik.class, query);
    }

    @BeleziKlice
    @Transactional
    public Long countUporabniki(QueryParameters query){
        return JPAUtils.queryEntitiesCount(em, Uporabnik.class, query);
    }

    @BeleziKlice
    @Transactional
    public List<Uporabnik> getUporabniki() {
        return em.createNamedQuery("Uporabnik.getAll", Uporabnik.class).getResultList();
    }

    @BeleziKlice
    @Transactional
    public Uporabnik getUporabnik(int uporabnikId) {
        return em.createNamedQuery("Uporabnik.getById", Uporabnik.class).setParameter("id", uporabnikId).getSingleResult();
    }

    @BeleziKlice
    @Transactional
    public void deleteUporabnik(int uporabnikId) {
        Uporabnik uporabnik = em.find(Uporabnik.class, uporabnikId);
        if(uporabnik != null){
            em.remove(uporabnik);
        }
    }

    @BeleziKlice
    @Transactional
    public void createUporabnik(Uporabnik uporabnik) {
        if(uporabnik != null){
            log.info("Ustvari uporabnika " + uporabnik.toString());
            em.persist(uporabnik);
        }
    }

    @BeleziKlice
    @Transactional
    public void updateUporabnik(Uporabnik uporabnik){
        Uporabnik test = em.find(Uporabnik.class, uporabnik.getId());
        if(test != null){
            em.merge(uporabnik);
        }
    }

    @BeleziKlice
    @Transactional
    public List<Uporabnik> getUporabnikiCriteriaApi() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Uporabnik> query = cb.createQuery(Uporabnik.class);
        Root<Uporabnik> rezultat = query.from(Uporabnik.class);
        query.select(rezultat);
        return em.createQuery(query).getResultList();
    }

    @PostConstruct
    public void logInit(){
        log.info("Inicializacija UporabnikZrno!");
    }

    @PreDestroy
    public void logDestroy(){
        log.info("Unicenje UporabnikZrno!");
    }
}