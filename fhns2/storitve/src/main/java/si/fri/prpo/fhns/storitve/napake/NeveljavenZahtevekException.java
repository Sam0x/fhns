package si.fri.prpo.fhns.storitve.napake;

public class NeveljavenZahtevekException extends RuntimeException {

    public NeveljavenZahtevekException(String sporocilo) {
        super(sporocilo);
    }
}
