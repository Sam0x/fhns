package si.fri.prpo.fhns.storitve.interceptorji;


import si.fri.prpo.fhns.storitve.zrna.BelezenjeKlicevZrno;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@Interceptor
@BeleziKlice
public class BelezenjeKlicevInterceptor {

    @Inject
    BelezenjeKlicevZrno belezenjeKlicevZrno;

    @AroundInvoke
    public Object beleziKlice(InvocationContext context) throws Exception {
        belezenjeKlicevZrno.povecajStKlicev();
        return  context.proceed();
    }

}