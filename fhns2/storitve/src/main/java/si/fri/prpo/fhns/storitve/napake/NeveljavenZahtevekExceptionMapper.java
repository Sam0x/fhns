package si.fri.prpo.fhns.storitve.napake;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NeveljavenZahtevekExceptionMapper implements ExceptionMapper<NeveljavenZahtevekException> {

    @Override
    public Response toResponse(NeveljavenZahtevekException exception) {
        return Response.status(Response.Status.BAD_REQUEST)
                .entity("{\"napaka\": \"" + exception.getMessage() + "\"}").build();
    }
}
