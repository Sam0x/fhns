package si.fri.prpo.fhns.storitve.zrna;

import javax.enterprise.context.ApplicationScoped;
import java.util.logging.Logger;

@ApplicationScoped
public class BelezenjeKlicevZrno {
    private  Logger log = Logger.getLogger(BelezenjeKlicevZrno.class.getName());

    private Integer stKlicev = 0;

    public void povecajStKlicev(){
        stKlicev++;
        log.info("St. klicev: " + stKlicev);
    }
}
