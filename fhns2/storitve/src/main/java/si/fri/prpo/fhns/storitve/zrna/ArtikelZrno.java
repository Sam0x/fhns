package si.fri.prpo.fhns.storitve.zrna;

import com.kumuluz.ee.rest.beans.QueryParameters;
import com.kumuluz.ee.rest.utils.JPAUtils;
import si.fri.prpo.fhns.entitete.Artikel;
import si.fri.prpo.fhns.entitete.OpisArtiklov;
import si.fri.prpo.fhns.storitve.App.AppProperties;
import si.fri.prpo.fhns.storitve.interceptorji.BeleziKlice;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import java.util.List;
import java.util.logging.Logger;

@ApplicationScoped
public class ArtikelZrno {

    Logger log = Logger.getLogger(ArtikelZrno.class.getName());

    private Client httpClient;

    private String baseUrl;

    @Inject
    private AppProperties appProperties;


    @PersistenceContext(unitName = "fhns2-jpa")
    private EntityManager em;

    @BeleziKlice
    public List<Artikel> getArtikle(QueryParameters query) {
        return JPAUtils.queryEntities(em, Artikel.class, query);
    }

    @BeleziKlice
    public Long countArtikel(QueryParameters query){
        return JPAUtils.queryEntitiesCount(em, Artikel.class, query);
    }

    @BeleziKlice
    public List<Artikel> getArtikle() {
        return em.createNamedQuery("Artikel.getAll", Artikel.class).getResultList();
    }

    @BeleziKlice
    public Artikel getArtikel(int artikelId) {
        return em.createNamedQuery("Artikel.getById", Artikel.class).setParameter("id", artikelId).getSingleResult();
    }

    @BeleziKlice
    @Transactional
    public void deleteArtikel(int artikelId) {
        Artikel artikel = em.find(Artikel.class, artikelId);
        if(artikel != null){
            em.remove(artikel);
        }
    }

    @BeleziKlice
    @Transactional
    public void createArtikel(Artikel artikel) {
        if(artikel != null){
            OpisArtiklov opis = pridobiOpis(artikel.getOpisId());
            artikel.setOpisArtiklov(opis);
            log.info("ustvarjen artikel: " + artikel.toString());
            em.persist(artikel);
        }
    }

    @BeleziKlice
    @Transactional
    public void updateArtikel(Artikel artikel){
        Artikel test = em.find(Artikel.class, artikel.getId());
        if(test != null){
            if(test.getOpisArtiklov()==null){
                OpisArtiklov opis = pridobiOpis(artikel.getOpisId());
                artikel.setOpisArtiklov(opis);
            }
            em.merge(artikel);
        }
    }

    @PostConstruct
    public void logInit(){
        log.info("Inicializacija ArtikelZrno!");

        httpClient = ClientBuilder.newClient();
        //baseUrl = ConfigurationUtil.getInstance().get("opis-artiklov.url").orElse("http://192.168.99.100:8081");
        //baseUrl = "http://localhost:8081";
        baseUrl = "https://fhns-opis-artikla.herokuapp.com/v1";
    }

    private OpisArtiklov pridobiOpis(Integer opisId) {
        if(appProperties.isOpisiArtiklovEnabled()) {
            try {
                return httpClient
                        .target(baseUrl + "/opisi/" + opisId)
                        .request().get(new GenericType<OpisArtiklov>() {
                        });
            }
            catch (WebApplicationException | ProcessingException e) {
                log.severe(e.getMessage());
                throw new InternalServerErrorException(e);
            }
        }
        else {
            return null;
        }
    }

    @PreDestroy
    public void logDestroy(){
        log.info("Unicenje ArtikelZrno!");
    }
}
