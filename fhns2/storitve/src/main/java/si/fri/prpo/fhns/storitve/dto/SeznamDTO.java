package si.fri.prpo.fhns.storitve.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SeznamDTO implements Serializable {

    private Integer UporabnikId;

    private String naziv;

    private String opis;

    private String status;

    private List<Integer> artikli = new ArrayList<Integer>();

    public Integer getUporabnikId() {
        return UporabnikId;
    }

    public SeznamDTO(int upId, String naziv, String opis, String status, List<Integer> artikli){
        this.UporabnikId = upId;
        this.naziv = naziv;
        this.opis = opis;
        this.status = status;
        this.artikli = artikli;
    }

    public void setUporabnikId(Integer uporabnikId) {
        UporabnikId = uporabnikId;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Integer> getArtikli() {
        return artikli;
    }

    public void setArtikli(List<Integer> artikli) {
        this.artikli = artikli;
    }
}