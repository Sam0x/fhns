package si.fri.prpo.fhns.storitve.App;

import com.kumuluz.ee.configuration.cdi.ConfigBundle;
import com.kumuluz.ee.configuration.cdi.ConfigValue;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
@ConfigBundle("opis-artiklov")
public class AppProperties {

    @ConfigValue(value = "enabled", watch = true)
    private boolean opisiArtiklovEnabled = true;


    public boolean isOpisiArtiklovEnabled() {
        return opisiArtiklovEnabled;
    }

    public void ponudnikiArtiklovEnabled(boolean opisiArtiklovEnabled) {
        this.opisiArtiklovEnabled = opisiArtiklovEnabled;
    }
}

