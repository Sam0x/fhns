package si.fri.prpo.fhns.storitve.dto;

import java.io.Serializable;

public class ArtikelDTO implements Serializable {

    private String naziv;

    private String opis;

    private String status;

    public ArtikelDTO(String naziv, String opis, String status){
        this.naziv = naziv;
        this.opis = opis;
        this.status = status;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}