package si.fri.prpo.fhns.storitve.zrna;
import com.kumuluz.ee.rest.beans.QueryParameters;
import com.kumuluz.ee.rest.utils.JPAUtils;
import si.fri.prpo.fhns.entitete.Seznam;
import si.fri.prpo.fhns.storitve.interceptorji.BeleziKlice;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

@ApplicationScoped
public class SeznamZrno {

    Logger log = Logger.getLogger(SeznamZrno.class.getName());

    private UUID uuid;

    @PersistenceContext(unitName = "fhns2-jpa")
    private EntityManager em;

    @BeleziKlice
    @Transactional
    public List<Seznam> getSeznam(QueryParameters query) {
        log.info("ApplicationScoped UUID: " + uuid.toString());
        return JPAUtils.queryEntities(em, Seznam.class, query);
    }

    @BeleziKlice
    @Transactional
    public Long countSeznam(QueryParameters query){
        return JPAUtils.queryEntitiesCount(em, Seznam.class, query);
    }

    @BeleziKlice
    @Transactional
    public List<Seznam> getSeznam() {
        log.info("ApplicationScoped UUID: " + uuid.toString());
        return em.createNamedQuery("Seznam.getAll", Seznam.class).getResultList();
    }

    @BeleziKlice
    @Transactional
    public Seznam getSeznam(int seznamId) {
        log.info("ApplicationScoped UUID: " + uuid.toString());
        return em.createNamedQuery("Seznam.getById", Seznam.class).setParameter("id", seznamId).getSingleResult();
    }

    @BeleziKlice
    @Transactional
    public void deleteSeznam(int seznamId) {
        log.info("ApplicationScoped UUID: " + uuid.toString());
        Seznam seznam = em.find(Seznam.class, seznamId);
        if(seznam != null){
            em.remove(seznam);
        }
    }

    @BeleziKlice
    @Transactional
    public void createSeznam(Seznam seznam) {
        log.info("ApplicationScoped UUID: " + uuid.toString());
        if(seznam != null){
            em.persist(seznam);
        }
    }

    @BeleziKlice
    @Transactional
    public void updateSeznam(Seznam seznam){
        log.info("ApplicationScoped UUID: " + uuid.toString());
        Seznam test = em.find(Seznam.class, seznam.getId());
        if(test != null){
            em.merge(seznam);
        }
    }

    @PostConstruct
    public void logInit(){
        log.info("Inicializacija SeznamZrno!");
        uuid = UUID.randomUUID();
    }

    @PreDestroy
    public void logDestroy(){
        log.info("Unicenje SeznamZrno!");
    }
}
