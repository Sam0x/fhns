package si.fri.prpo.fhns.storitve.dto;

import si.fri.prpo.fhns.entitete.OpisArtiklov;

import java.io.Serializable;

public class DodajArtikelNaSeznamDTO implements Serializable {
    String artikelNaziv;
    OpisArtiklov artikelOpis;
    Integer seznamId;

    public String getArtikelNaziv() {
        return artikelNaziv;
    }

    public OpisArtiklov getArtikelOpis() {
        return artikelOpis;
    }

    public Integer getSeznamId() {
        return seznamId;
    }

    public boolean setArtikelNaziv(String artikelNaziv) {
        this.artikelNaziv = artikelNaziv;
        return true;
    }

    public boolean setArtikelOpis(OpisArtiklov artikelOpis) {
        this.artikelOpis = artikelOpis;
        return true;
    }

    public boolean setSeznamId(Integer seznamId) {
        this.seznamId = seznamId;
        return true;
    }
}
