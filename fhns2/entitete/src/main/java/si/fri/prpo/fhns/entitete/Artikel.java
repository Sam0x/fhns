package si.fri.prpo.fhns.entitete;

import javax.persistence.*;

@Entity(name = "artikel")
@NamedQueries(
        value = {
                @NamedQuery(name = "Artikel.getAll", query = "SELECT a FROM artikel a"),
                @NamedQuery(name = "Artikel.getById", query = "SELECT a FROM artikel a WHERE a.id = :id"),
                @NamedQuery(name = "Artikel.getAktivniArtikli", query = "SELECT a FROM artikel a WHERE a.status LIKE 'Aktivni'"),
                @NamedQuery(name = "Artikel.getAllNaziv", query = "SELECT a.naziv FROM artikel a")
        })
public class Artikel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String naziv;

    @Transient
    private OpisArtiklov opisArtiklov;

    private String status;

    private int opisId;

    @ManyToOne
    @JoinTable(name = "seznami_id")
    private Seznam seznam;

    public Artikel() {
        // default constructor
    }

    public Artikel(String naziv, OpisArtiklov opisArtiklov, String status, Seznam seznam){
        this.naziv = naziv;
        this.opisArtiklov = opisArtiklov;
        this.status = status;
        this.seznam = seznam;
    }

    // getter in setter metode

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public OpisArtiklov getOpisArtiklov() {
        return opisArtiklov;
    }

    public void setOpisArtiklov(OpisArtiklov opisArtiklov) {
        this.opisArtiklov = opisArtiklov;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Seznam getSeznami() {
        return seznam;
    }

    public void setSeznami(Seznam seznam) {
        this.seznam = seznam;
    }

    @Override
    public String toString() {
        String o;
        if(opisArtiklov==null) o = "brez opisa";
        else o = opisArtiklov.getOpis();
        String result = String.format(
                "Artikel (id=%d, naziv=%s, opis=%s, status=%s)%n <BR>",
                id, naziv, o, status);
        return result;
    }

    public int getOpisId() {
        return opisId;
    }

    public void setOpisId(int opisId) {
        this.opisId = opisId;
    }
}