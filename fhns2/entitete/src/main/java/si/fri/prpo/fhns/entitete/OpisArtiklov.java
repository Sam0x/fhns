package si.fri.prpo.fhns.entitete;

public class OpisArtiklov {

    private Integer artikelId;
    private String naziv;
    private String opis;

    public OpisArtiklov(Integer artikelId, String naziv, String opis) {
        this.artikelId = artikelId;
        this.naziv = naziv;
        this.opis = opis;
    }

    public OpisArtiklov() {}

    public Integer getArtikelId() {
        return artikelId;
    }

    public String getNaziv() {
        return naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setArtikelId(Integer ponudnikId) {
        this.artikelId = artikelId;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }
}

