package si.fri.prpo.fhns.entitete;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "seznam")
@NamedQueries(value =
        {
                @NamedQuery(name = "Seznam.getAll", query = "SELECT s FROM seznam s"),
                @NamedQuery(name = "Seznam.getById", query = "SELECT s FROM seznam s WHERE s.id = :id"),
                @NamedQuery(name = "Seznam.countAktivniSeznami", query = "SELECT COUNT (s) FROM seznam s WHERE s.status LIKE 'Aktivni'"),
                @NamedQuery(name = "Seznam.allNaziv", query = "SELECT s.naziv FROM artikel s")
        })
public class Seznam {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String naziv;

    private String opis;

    private String status;

    @ManyToMany
    @JoinTable(name = "artikel_id")
    private List<Artikel> artikli = new ArrayList<Artikel>();

    @OneToMany
    @JoinTable(name = "uporabniki_id")
    private List<Uporabnik> uporabniki;

    public Seznam() {
        // default constructor
    }

    public Seznam(String naziv, String opis, String status, List<Artikel> artikli, List<Uporabnik> uporabniki){
        this.naziv = naziv;
        this.opis = opis;
        this.status = status;
        this.artikli = artikli;
        this.uporabniki = uporabniki;
    }

    // getter in setter metode

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Artikel> getArtikli() {
        return artikli;
    }

    public void setArtikli(List<Artikel> artikli) {
        this.artikli = artikli;
    }

    public List<Uporabnik> getUporabniki() {
        return uporabniki;
    }

    public void setUporabniki(List<Uporabnik> uporabniki) {
        this.uporabniki = uporabniki;
    }

    @Override
    public String toString() {
        String result = String.format(
                "Seznam (id=%d, naziv=%s, opis=%s, status=%s)%n <BR>",
                id, naziv, opis, status);
        if (artikli != null) {
            for(Artikel a : artikli) {
                result += ("-->" + a.toString());
            }
        }
        if (uporabniki != null) {
            for(Uporabnik u : uporabniki) {
                result += ("-->" + u.toString());
            }
        }

        return result;
    }
}