package si.fri.prpo.fhns.entitete;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity(name = "uporabnik")
@NamedQueries(value =
        {
                @NamedQuery(name = "Uporabnik.getAll", query = "SELECT u FROM uporabnik u"),
                @NamedQuery(name = "Uporabnik.getById", query = "SELECT u FROM uporabnik  u WHERE u.id = :id"),
                @NamedQuery(name = "Uporabnik.getByUporabniskoIme", query = "SELECT u FROM uporabnik u WHERE u.uporabnisko_ime = :uporabnisko_ime"),
                @NamedQuery(name = "Uporabnik.getWithoutEmail", query = "SELECT u FROM uporabnik u WHERE u.email = NULL"),
                @NamedQuery(name = "Uporabnik.getAllUporabIme", query = "SELECT u.uporabnisko_ime FROM uporabnik u"),
                @NamedQuery(name = "Uporabnik.deleteById", query = "DELETE FROM uporabnik  u WHERE u.id = :id")
        })
public class Uporabnik {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String ime;

    private String priimek;

    private String uporabnisko_ime;

    private String email;

    @ManyToMany
    @JoinTable(name = "seznam_id")
    private List<Seznam> seznami = new ArrayList<Seznam>();

    public Uporabnik() {
        // default constructor
    }

    public Uporabnik(String ime, String priimek, String uporabnisko_ime, String email, List<Seznam> seznami){
        this.ime = ime;
        this.priimek = priimek;
        this.uporabnisko_ime = uporabnisko_ime;
        this.email = email;
        this.seznami = seznami;
    }

    // getter in setter metode

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPriimek() {
        return priimek;
    }

    public void setPriimek(String priimek) {
        this.priimek = priimek;
    }

    public String getUporabnisko_ime() {
        return uporabnisko_ime;
    }

    public void setUporabnisko_ime(String uporabnisko_ime) {
        this.uporabnisko_ime = uporabnisko_ime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Seznam> getSeznami() {
        return seznami;
    }

    public void setSeznami(List<Seznam> seznami) {
        this.seznami = seznami;
    }

    @Override
    public String toString() {
        String result = String.format(
                "Uporabnik (id=%d, ime=%s, priimek=%s, uporabnisko_ime=%s, email=%s)%n <BR>",
                id, ime, priimek, uporabnisko_ime, email);
        if (seznami != null) {
            for(Seznam s : seznami) {
                result += ("->" + s.toString());
            }
        }

        return result;
    }
}