package si.fri.prpo.fhns.api;

import si.fri.prpo.fhns.entitete.Seznam;
import si.fri.prpo.fhns.entitete.Uporabnik;
import si.fri.prpo.fhns.storitve.dto.DodajArtikelNaSeznamDTO;
import si.fri.prpo.fhns.storitve.zrna.SeznamZrno;
import si.fri.prpo.fhns.storitve.zrna.UporabnikiZrno;
import si.fri.prpo.fhns.storitve.zrna.UpravljanjeSeznamovZrno;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Random;

@WebServlet(value = "/servlet")
public class JPAServlet extends HttpServlet {

    @Inject
    private UporabnikiZrno uporabnikiZrno;

    @Inject
    private SeznamZrno seznamiZrno;

    @Inject
    private UpravljanjeSeznamovZrno upravljanjeSeznamovZrno;

    String[] test = {"kruh", "jajca", "mleko", "cips", "pivo", "radler"};
    static int counter = -1;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        counter+=1;

        PrintWriter out = resp.getWriter();

        if(counter==0){
            ustvariNovegaUporabnika();
        }
        out.println("<h1> JPAServlet - testing </h1>");

        Random rand = new Random();
        dodajanjeArtiklovNaSeznam(test[rand.nextInt(6)], 0);
        dodajanjeArtiklovNaSeznam(test[rand.nextInt(6)], 1);

        out.println(izpisiVseUporabnike());
        out.println(izpisiVseSezname());

    }
    private void ustvariNovegaUporabnika(){
        Uporabnik nov = new Uporabnik("Jose", "Mouriniho", "SriNil", "jos.mou@united.com", null);
        uporabnikiZrno.createUporabnik(nov);
    }

    private String izpisiVseUporabnike(){
        StringBuilder sb = new StringBuilder();
        List<Uporabnik> uporabniki = uporabnikiZrno.getUporabniki();
        if (uporabniki != null && !uporabniki.isEmpty()){
            for (Uporabnik u : uporabniki) {
                sb.append(u.toString()).append("\n");
            }
        }
        return sb.toString();
    }

    private void dodajanjeArtiklovNaSeznam(String naziv, Integer seznam) {
        DodajArtikelNaSeznamDTO dto = new DodajArtikelNaSeznamDTO();
        dto.setArtikelNaziv(naziv);
        dto.setSeznamId(seznamiZrno.getSeznam().get(seznam).getId());
        upravljanjeSeznamovZrno.dodajArtikelNaSeznam(dto);
    }

    private String izpisiVseSezname(){
        StringBuilder sb = new StringBuilder();
        List<Seznam> seznami = seznamiZrno.getSeznam();
        if (seznami != null && !seznami.isEmpty()){
            for (Seznam u : seznami) {
                sb.append(u.toString()).append("\n");
            }
        }
        return sb.toString();
    }
}