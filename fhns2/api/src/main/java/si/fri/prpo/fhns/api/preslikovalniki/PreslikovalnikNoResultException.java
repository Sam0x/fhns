package si.fri.prpo.fhns.api.preslikovalniki;

import si.fri.prpo.fhns.entitete.Napaka;
import javax.persistence.NoResultException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class PreslikovalnikNoResultException implements ExceptionMapper<NoResultException> {

    @Override
    public Response toResponse(NoResultException e) {
        return Response
                .status(Response.Status.NOT_FOUND)
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .entity(new Napaka(404, e.getMessage()))
                .build();
    }

}


