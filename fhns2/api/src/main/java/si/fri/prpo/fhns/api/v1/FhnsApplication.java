package si.fri.prpo.fhns.api.v1;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import si.fri.prpo.fhns.api.preslikovalniki.PreslikovalnikNoResultException;
import si.fri.prpo.fhns.api.preslikovalniki.PreslikovalnikNotFoundException;
import si.fri.prpo.fhns.api.preslikovalniki.PreslikovalnikQueryFormatException;

import javax.annotation.security.DeclareRoles;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Set;

@OpenAPIDefinition(info = @Info(title = "Rest API", version = "v1", contact = @Contact(), license = @License(), description = "API za FulHudeNakupovalneSezname"), security = @SecurityRequirement(name = "openid-connect"), servers = @Server(url ="http://localhost:8080/v1"))
@DeclareRoles({"skrbnik", "administrator", "user"})
@ApplicationPath("v1")
public class FhnsApplication extends Application {
}
