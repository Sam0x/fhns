package si.fri.prpo.fhns.api.preslikovalniki;

import com.kumuluz.ee.rest.exceptions.QueryFormatException;
import si.fri.prpo.fhns.entitete.Napaka;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class PreslikovalnikQueryFormatException implements ExceptionMapper<QueryFormatException> {

    @Override
    public Response toResponse(QueryFormatException e) {
        return Response
                .status(Response.Status.BAD_REQUEST)
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .entity(new Napaka(400, String.format("Polje: %s, napaka: %s", e.getField(), e.getReason())))
                .build();
    }

}


