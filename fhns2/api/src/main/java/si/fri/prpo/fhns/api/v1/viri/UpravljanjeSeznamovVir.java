package si.fri.prpo.fhns.api.v1.viri;
import si.fri.prpo.fhns.storitve.dto.ArtikelDTO;
import si.fri.prpo.fhns.storitve.dto.SeznamDTO;
import si.fri.prpo.fhns.storitve.zrna.UpravljanjeSeznamovZrno;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("upravljanjeSeznamov")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class UpravljanjeSeznamovVir {

    @Inject
    private UpravljanjeSeznamovZrno upravljanjeSeznamovZrno;

    @POST
    @Path("/dodajSeznam")
    @RolesAllowed("administrator")
    public Response dodajSeznam(SeznamDTO seznam) {
        upravljanjeSeznamovZrno.dodajSeznam(seznam);
        return null;
    }

    @POST
    @Path("/odstraniKoncaneSezname")
    @RolesAllowed("administrator")
    public Response odstraniKoncaneSezname() {
        upravljanjeSeznamovZrno.odstraniKoncaneSezname();
        return null;
    }
    @POST
    @Path("dodajNovArtikel")
    @RolesAllowed("administrator")
    public Response dodajNovArtikel(ArtikelDTO artikel) {
        upravljanjeSeznamovZrno.dodajNovArtikel(artikel);
        return null;
    }

}

