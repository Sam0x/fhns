package si.fri.prpo.fhns.api.preslikovalniki;

import si.fri.prpo.fhns.entitete.Napaka;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class PreslikovalnikNotFoundException implements ExceptionMapper<NotFoundException> {

    @Override
    public Response toResponse(NotFoundException e) {
        return Response
                .status(Response.Status.NOT_FOUND)
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .entity(new Napaka(e.getResponse().getStatus(), e.getMessage()))
                .build();
    }

}



