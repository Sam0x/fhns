package si.fri.prpo.fhns.api.v1.viri;

import com.kumuluz.ee.rest.beans.QueryParameters;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import si.fri.prpo.fhns.entitete.Napaka;
import si.fri.prpo.fhns.entitete.Seznam;
import si.fri.prpo.fhns.storitve.zrna.SeznamZrno;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Path("seznam")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class SeznamVir {

    @Inject
    private SeznamZrno seznamZrno;

    @Context
    protected UriInfo uriInfo;

    @Operation(
            summary = "Vrni sezname",
            description = "Klic vrne sezname, omejene glede na query parametre.",
            tags = "tocke",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Seznam izbranih seznamov",
                            content = @Content(schema = @Schema(implementation = Seznam.class))
                    ), @ApiResponse(responseCode = "400", description = "Napaka v zahtevi", content = @Content(schema = @Schema(implementation = Napaka.class)))
            },
            parameters = {
                    @Parameter(name = "offset", description = "Offset od začetne pozicije izbranih vnosov", in = ParameterIn.QUERY, schema = @Schema(type = "int")),
                    @Parameter(name = "limit", description = "Največja količina vrnjenih seznamov", in = ParameterIn.QUERY, schema = @Schema(type = "int")),
                    @Parameter(name = "order", description = "Urejenost odgovora", in = ParameterIn.QUERY),
            }
    )
    @GET
    @PermitAll
    public Response getSeznam(){
        QueryParameters query = QueryParameters.query(uriInfo.getRequestUri().getQuery()).build();
        List<Seznam> seznam = seznamZrno.getSeznam(query);
        Long all = seznamZrno.countSeznam(query);
        return Response.status(Response.Status.OK).entity(seznam).header("X-Total-Count", all).build();
    }

    @Operation(
            summary = "Vrni seznam",
            description = "Vrne seznam s podanim {id}",
            tags = "seznam",
            responses = {
                    @ApiResponse(responseCode = "200", description = "JSON objekt seznama", content = @Content(schema = @Schema(implementation = Seznam.class))),
                    @ApiResponse(responseCode = "404", description = "Seznam s podanim ID ne obstaja", content = @Content(schema = @Schema(implementation = Napaka.class)))
            }
    )
    @GET
    @Path("/{id}")
    @PermitAll
    public Response getSeznam(@PathParam("id") Integer id) {
        Seznam seznam = seznamZrno.getSeznam(id);
        return seznam != null
                ? Response.status(Response.Status.OK).entity(seznam).build()
                : Response.status(Response.Status.NOT_FOUND).build();
    }

    @Operation(
            summary = "Dodaj seznam",
            description = "Doda nov seznam",
            tags = "seznam",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Vnos seznama uspešen"),
                    @ApiResponse(responseCode = "400", description = "Nepravilno ali manjkajoče telo zahteve")
            }
    )
    @POST
    @Path("/create")
    @RolesAllowed("administrator")
    public Response createSeznam(Seznam seznam) {
        seznamZrno.createSeznam(seznam);
        return null;
    }

    @Operation(
            summary = "Posodobi seznam",
            description = "Posodobi seznam s podanim {id}",
            tags = "seznam",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Posodobitev seznama uspešna"),
                    @ApiResponse(responseCode = "404", description = "Seznam s podanim ID ne obstaja", content = @Content(schema = @Schema(implementation = Napaka.class))),
                    @ApiResponse(responseCode = "400", description = "Nepravilno ali manjkajoče telo zahteve")
            }
    )
    @POST
    @Path("/update")
    @RolesAllowed("administrator")
    public Response updateSeznam(Seznam seznam) {
        seznamZrno.updateSeznam(seznam);
        return null;
    }

    @Operation(
            summary = "Odstrani seznam",
            description = "Odstrani seznam s podanim {id}",
            tags = "seznam",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Izbris seznama uspešen"),
                    @ApiResponse(responseCode = "404", description = "Seznam s podanim ID ne obstaja", content = @Content(schema = @Schema(implementation =  Napaka.class)))
            }
    )
    @DELETE
    @Path("/{id}")
    @RolesAllowed("administrator")
    public Response deleteSeznam(@PathParam("id") Integer id) {
        seznamZrno.deleteSeznam(id);
        return null;

    }

}

