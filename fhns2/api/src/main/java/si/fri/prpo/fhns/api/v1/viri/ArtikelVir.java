package si.fri.prpo.fhns.api.v1.viri;

import com.kumuluz.ee.rest.beans.QueryParameters;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import si.fri.prpo.fhns.entitete.Artikel;
import si.fri.prpo.fhns.entitete.Napaka;
import si.fri.prpo.fhns.storitve.zrna.ArtikelZrno;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Path("artikel")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class ArtikelVir {


    @Inject
    private ArtikelZrno artikelZrno;

    @Context
    protected UriInfo uriInfo;

    @Operation(
            summary = "Vrni artikel",
            description = "Klic vrne artikel, glede na query parametre.",
            tags = "artikel",
            parameters = {
                    @Parameter(name = "offset", description = "Offset od začetne pozicije izbranih artiklov", in = ParameterIn.QUERY, schema = @Schema(type = "int")),
                    @Parameter(name = "limit", description = "Največja količina vrnjenih artiklov", in = ParameterIn.QUERY, schema = @Schema(type = "int")),
                    @Parameter(name = "order", description = "Urejenost artiklov, podana v SQL ORDER BY obliki", in = ParameterIn.QUERY),
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "Seznam izbranih artiklov",
                            content = @Content(schema = @Schema(implementation = Artikel.class)),
                            headers = {
                                    @Header(name = "X-Total-Count", schema = @Schema(type = "int"))
                            }
                    ),
                    @ApiResponse(responseCode = "400", description = "Napaka v zahtevi", content = @Content(schema = @Schema(implementation = Napaka.class)))
            }
    )
    @GET
    @PermitAll
    public Response getArtikel(){
        QueryParameters query = QueryParameters.query(uriInfo.getRequestUri().getQuery()).build();
        List<Artikel> artikel = artikelZrno.getArtikle(query);
        Long all = artikelZrno.countArtikel(query);
        return Response.status(Response.Status.OK).entity(artikel).header("X-Total-Count", all).build();
    }

    @Operation(
            summary = "Vrni artikel",
            description = "Vrne artikel s podanim {id}",
            tags = "artikel",
            responses = {
                    @ApiResponse(responseCode = "200", description = "JSON objekt artikel", content = @Content(schema = @Schema(implementation = Artikel.class))),
                    @ApiResponse(responseCode = "404", description = "Artikel s podanim ID ne obstaja", content = @Content(schema = @Schema(implementation = Napaka.class)))
            }
    )
    @GET
    @Path("/{id}")
    @PermitAll
    public Response getArtikel(@PathParam("id") Integer id) {
        Artikel artikel = artikelZrno.getArtikel(id);
        artikelZrno.updateArtikel(artikel);
        return artikel != null
                ? Response.status(Response.Status.OK).entity(artikel).build()
                : Response.status(Response.Status.NOT_FOUND).build();
    }

    @Operation(
            summary = "Keriraj artikel",
            description = "Kreira nov artikel",
            tags = "artikel",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Vnos artikla uspešen"),
                    @ApiResponse(responseCode = "400", description = "Nepravilno ali manjkajoče telo zahteve")
            }
    )
    @POST
    @Path("/create")
    @RolesAllowed("administrator")
    public Response createArtikel(Artikel artikel) {
        artikelZrno.createArtikel(artikel);
        return null;
    }

    @Operation(
            summary = "Posodobi artikel",
            description = "Posodobi artikel s podanim {id}",
            tags = "artikel",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Posodobitev artikla uspešna"),
                    @ApiResponse(responseCode = "404", description = "Artikel s podanim ID ne obstaja", content = @Content(schema = @Schema(implementation = Napaka.class))),
                    @ApiResponse(responseCode = "400", description = "Nepravilno ali manjkajoče telo zahteve")
            }
    )
    @POST
    @Path("/update")
    @RolesAllowed("administrator")
    public Response updateArtikel(Artikel artikel) {
        artikelZrno.updateArtikel(artikel);
        return null;
    }

    @Operation(
            summary = "Odstrani artikel",
            description = "Odstrani artikel s podanim {id}",
            tags = "artikel",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Izbris artikla uspešen"),
                    @ApiResponse(responseCode = "404", description = "Storitev s podanim ID ne obstaja", content = @Content(schema = @Schema(implementation =  Napaka.class)))
            }
    )
    @DELETE
    @Path("/{id}")
    @RolesAllowed("administrator")
    public Response deleteArtikel(@PathParam("id") Integer id) {
        artikelZrno.deleteArtikel(id);
        return null;

    }

}

