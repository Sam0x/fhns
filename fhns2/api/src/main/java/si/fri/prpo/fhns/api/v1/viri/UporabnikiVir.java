package si.fri.prpo.fhns.api.v1.viri;

import com.kumuluz.ee.rest.beans.QueryParameters;
import com.kumuluz.ee.security.annotations.Secure;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import si.fri.prpo.fhns.entitete.Napaka;
import si.fri.prpo.fhns.entitete.Uporabnik;
import si.fri.prpo.fhns.storitve.zrna.UporabnikiZrno;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Secure
@Path("uporabniki")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class UporabnikiVir {

    @Inject
    UporabnikiZrno uporabnikiZrno;

    @Context
    protected UriInfo uriInfo;

    @Operation(
            summary = "Vrni uporabnike",
            description = "Klic vrne uporabnike, glede na query parametre.",
            tags = "uporabniki",
            parameters = {
                    @Parameter(name = "offset", description = "Offset od začetne pozicije izbranih uporabnikov", in = ParameterIn.QUERY, schema = @Schema(type = "int")),
                    @Parameter(name = "limit", description = "Največja količina vrnjenih uporabnikov", in = ParameterIn.QUERY, schema = @Schema(type = "int")),
                    @Parameter(name = "order", description = "Urejenost entitet, podana v SQL ORDER BY obliki", in = ParameterIn.QUERY),
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "Seznam izbranih uporabnikov",
                            content = @Content(schema = @Schema(implementation = Uporabnik.class))
                    ),
                    @ApiResponse(responseCode = "400", description = "Napaka v zahtevi", content = @Content(schema = @Schema(implementation = Napaka.class)))
            }
    )
    @GET
    @PermitAll
    public Response getUporabniki(){
        QueryParameters query = QueryParameters.query(uriInfo.getRequestUri().getQuery()).build();
        List<Uporabnik> uporabniki = uporabnikiZrno.getUporabniki(query);
        Long all = uporabnikiZrno.countUporabniki(query);
        return Response.status(Response.Status.OK).entity(uporabniki).header("X-Total-Count", all).build();
    }

    @Operation(
            summary = "Vrni uporabnika",
            description = "Vrne uporabnika s podanim {id}",
            tags = "uporabniki",
            responses = {
                    @ApiResponse(responseCode = "200", description = "JSON objekt uporabnika", content = @Content(schema = @Schema(implementation = Uporabnik.class))),
                    @ApiResponse(responseCode = "404", description = "Uporabnik s podanim ID ne obstaja", content = @Content(schema = @Schema(implementation = Napaka.class)))
            }
    )
    @Path("/{id}")
    @GET
    @PermitAll
    public Response getUporabnik(@PathParam("id") Integer id) {
        Uporabnik uporabnik = uporabnikiZrno.getUporabnik(id);
        return uporabnik != null
                ? Response.status(Response.Status.OK).entity(uporabnik).build()
                : Response.status(Response.Status.NOT_FOUND).build();
    }

    @Operation(
            summary = "Dodaj uporabnika",
            description = "Doda novega uporabnika",
            tags = "uporabniki",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Vnos uporabnika uspešen"),
                    @ApiResponse(responseCode = "203", description = "Napačen vnos uporabnika"),
                    @ApiResponse(responseCode = "400", description = "Nepravilno ali manjkajoče telo zahteve")
            }
    )
    @POST
    @Path("/create")
    @RolesAllowed("administrator")
    public Response createUporabnik(Uporabnik uporabnik) {
        uporabnikiZrno.createUporabnik(uporabnik);
        return null;
    }

    @Operation(
            summary = "Pododobi uporabnika",
            description = "Posodobi stanje uporabnika s podanim {id}",
            tags = "uporabniki",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Posodobitev uporabnika uspešna"),
                    @ApiResponse(responseCode = "404", description = "Uporabnik s podanim ID ne obstaja", content = @Content(schema = @Schema(implementation = Napaka.class))),
                    @ApiResponse(responseCode = "400", description = "Nepravilno ali manjkajoče telo zahteve")
            }
    )
    @POST
    @Path("/update")
    @RolesAllowed("administrator")
    public Response updateUporabnik(Uporabnik uporabnik) {
        uporabnikiZrno.updateUporabnik(uporabnik);
        return null;
    }


    @Operation(
            summary = "Odstrani uporabnika",
            description = "Odstrani uporabnika s podanim {id}",
            tags = "uporabniki",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Izbris uporabnika uspešen"),
                    @ApiResponse(responseCode = "404", description = "Uporabnik s podanim ID ne obstaja", content = @Content(schema = @Schema(implementation =  Napaka.class)))
            }
    )
    @DELETE
    @Path("/{id}")
    @RolesAllowed("administrator")
    public Response deleteUporabnik(@PathParam("id") Integer id) {
        uporabnikiZrno.deleteUporabnik(id);
        return Response.status(Response.Status.ACCEPTED).build();
    }

}
